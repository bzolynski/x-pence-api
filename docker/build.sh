#!/usr/bin/env bash
docker build -t x_pence/php images/php
docker build -t x_pence/nginx images/nginx
docker build -t x_pence/mysql images/mysql
