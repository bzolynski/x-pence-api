<?php

namespace DomainLogic\Test;

use DomainLogic\Entity\User\User;
use DomainLogic\Entity\User\InMemoryUserRepository;

class TestUser extends \PHPUnit_Framework_TestCase
{
    /** @var User */
    private $user;

    private $userRepository;

    public function setUp()
    {
        $this->userRepository = new InMemoryUserRepository();
        $this->user = $this->userRepository->find(1);
    }

    public function testInstantiateObject()
    {
        $this->assertInstanceOf(User::class, $this->user);
        $this->assertEquals('Ivo', $this->user->getName());
    }

    public function testCreate()
    {
        $user = new User('Beatrix');
        $this->assertEquals('Beatrix', $user->getName());
    }
}
