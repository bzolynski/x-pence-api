<?php

namespace DomainLogic\Test;

use DomainLogic\Entity\ExpenseEntry\ExpenseEntry;
use DomainLogic\Entity\ExpenseEntry\InMemoryExpenseEntryRepository;
use DomainLogic\Entity\User\InMemoryUserRepository;
use ArchitectureLogic\ValueObject\Money;

class TestExpenseEntry extends \PHPUnit_Framework_TestCase
{
    private $user;

    /**
     * @var InMemoryExpenseEntryRepository
     */
    private $expenseEntryRepository;

    /**
     * @var InMemoryUserRepository
     */
    private $userRepository;

    public function setUp()
    {
        $this->expenseEntryRepository = new InMemoryExpenseEntryRepository();
        $this->userRepository = new InMemoryUserRepository();
        $this->user = $this->userRepository->find(1);
    }

    public function testInstantiateObject()
    {
        $expenseEntry = new ExpenseEntry($this->user, new Money(1));
        $this->assertInstanceOf(ExpenseEntry::class, $expenseEntry);
    }

    public function testCreate()
    {
        $value = 150.45;
        $expenseEntry = new ExpenseEntry($this->user, new Money($value));
        $this->assertEquals($value, $expenseEntry->getValue());
    }

    public function testFind()
    {
        $this->assertEquals(3, count($this->expenseEntryRepository->findByUser($this->user)));
    }
}
