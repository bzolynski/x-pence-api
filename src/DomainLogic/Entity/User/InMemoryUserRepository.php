<?php

declare(strict_types=1);

namespace DomainLogic\Entity\User;

class InMemoryUserRepository
{
    /** @var array */
    private $users = array();

    public function __construct()
    {
        foreach ($this->getData() as $data) {
            $user = new User($data['name']);
            $user->setId($data['id']);
            $this->users[] = $user;
        }
    }

    /**
     * @param integer $id
     * @return User|null
     */
    public function find($id)
    {
        foreach($this->users as $user) {
            /** @var $user User */
            if ($user->getId() === (int)$id) {
                return $user;
            }
        }
        return null;
    }

    public function findAll(): array
    {
        return $this->users;
    }

    public function add(User $expenseEntry)
    {

    }

    public function remove($id)
    {

    }

    /**
     * This data can be shared for both, in-memory repositories and data fixtures
     *
     * @return array
     */
    public function getData(): array
    {
        return array(
            array(
                'name' => 'Ivo',
                'id' => 1
            ),
            array(
                'name' => 'Lorelei',
                'id' => 2
            )
        );
    }
}
