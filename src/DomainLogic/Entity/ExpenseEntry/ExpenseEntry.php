<?php

declare(strict_types=1);

namespace DomainLogic\Entity\ExpenseEntry;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

use ArchitectureLogic\ValueObject\Money;
use ArchitectureLogic\Exception\NotAllowedValueException;

use DomainLogic\Entity\User\User;

/**
 * @ORM\Entity(repositoryClass="DomainLogic\Entity\ExpenseEntry\ExpenseEntryRepository")
 * @ORM\Table(name="expense_entries")
 */
class ExpenseEntry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Groups({"default"})
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="DomainLogic\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Groups({"default"})
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Groups({"default"})
     */
    private $created;

    /**
     * @param User $user
     * @param $money
     */
    public function __construct(User $user, Money $money)
    {
        $this->setValue($money->getValue());
        $this->setUser($user);
    }

    /**
     * Set id for in-memory repository test purposes
     *
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param float $value
     * @throws NotAllowedValueException
     */
    private function setValue(float $value)
    {
        $expense = new Money($value);
        $this->value = $expense->getValue();
    }

    /**
     * @param User $user
     */
    private function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
