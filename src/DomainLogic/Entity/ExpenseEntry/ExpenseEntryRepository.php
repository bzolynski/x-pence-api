<?php

declare(strict_types=1);

namespace DomainLogic\Entity\ExpenseEntry;

use Doctrine\ORM\EntityRepository;

class ExpenseEntryRepository extends EntityRepository
{
    public function findByUser($user): array
    {
        $qb = $this
            ->createQueryBuilder('ExpenseEntry')
            ->select('ExpenseEntry')
            ->where('ExpenseEntry.user = :user')
            ->setParameter('user', $user);

        return $qb->getQuery()->getResult();
    }
}
