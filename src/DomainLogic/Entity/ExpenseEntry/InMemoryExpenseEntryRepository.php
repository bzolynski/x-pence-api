<?php

declare(strict_types=1);

namespace DomainLogic\Entity\ExpenseEntry;

use DomainLogic\Entity\User\InMemoryUserRepository;
use DomainLogic\Entity\User\User;
use ArchitectureLogic\ValueObject\Money;

class InMemoryExpenseEntryRepository
{
    /** @var array */
    private $entries = array();

    public function __construct()
    {
        $userRepository = new InMemoryUserRepository();
        $id = 0;
        foreach($this->getData() as $data) {
            $userId = filter_var($data['userReference'], FILTER_SANITIZE_NUMBER_INT);
            $user = $userRepository->find($userId);

            $entry = new ExpenseEntry($user, new Money($data['value']));
            $id += 1;
            $entry->setId($id);

            $this->entries[] = $entry;
        }
    }

    /**
     * @param integer $id
     * @return ExpenseEntry|null
     */
    public function find($id)
    {
        foreach ($this->entries as $entry) {
            /** @var $entry ExpenseEntry */
            if ($entry->getId() === $id) {
                return $entry;
            }
        }
        return array();
    }

    public function findAll(): array
    {
        return $this->entries;
    }

    /**
     * @param User $user
     * @return array
     */
    public function findByUser($user): array
    {
        $result = array();
        foreach($this->entries as $entry) {
            /** @var $entry ExpenseEntry */
            if ($entry->getUser()->getId() === $user->getId()) {
                $result[] = $entry;
            }
        }
        return $result;
    }

    public function add(ExpenseEntry $expenseEntry)
    {

    }

    public function remove($id)
    {

    }

    /**
     * This data can be shared for both, in-memory repositories and data fixtures
     *
     * @return array
     */
    public function getData(): array
    {
        return array(
            array(
                'userReference' => 1,
                'value' => 150.45
            ),
            array(
                'userReference' => 1,
                'value' => 12.8
            ),
            array(
                'userReference' => 1,
                'value' => 12.8
            ),
            array(
                'userReference' => 2,
                'value' => 135
            ),
            array(
                'userReference' => 2,
                'value' => 57.55
            ),
        );
    }
}
