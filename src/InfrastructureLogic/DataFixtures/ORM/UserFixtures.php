<?php

declare(strict_types=1);

namespace DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use DomainLogic\Entity\User\InMemoryUserRepository;
use DomainLogic\Entity\User\User;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach($this->getData() as $data) {
            $user = new User($data['name']);
            $manager->persist($user);
            $userReferenceName = 'user' . $data['id'];
            $this->addReference($userReferenceName, $user);
        }
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 1;
    }

    public function getData(): array
    {
        $inMemoryUserRepository = new InMemoryUserRepository();
        return $inMemoryUserRepository->getData();
    }
}
