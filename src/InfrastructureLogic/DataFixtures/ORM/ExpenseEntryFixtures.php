<?php

declare(strict_types=1);

namespace DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use DomainLogic\Entity\ExpenseEntry\InMemoryExpenseEntryRepository;
use DomainLogic\Entity\User\User;
use DomainLogic\Entity\ExpenseEntry\ExpenseEntry;
use ArchitectureLogic\ValueObject\Money;

class ExpenseEntryFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach($this->getData() as $data) {
            $userReferenceName = 'user' . $data['userReference'];
            /** @var User $user */
            $user = $this->getReference($userReferenceName);
            $expenseEntry = new ExpenseEntry($user, new Money($data['value']));
            $manager->persist($expenseEntry);
        }
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 2;
    }

    private function getData(): array
    {
        $inMemoryExpenseEntryRepository = new InMemoryExpenseEntryRepository();
        return $inMemoryExpenseEntryRepository->getData();
    }
}
