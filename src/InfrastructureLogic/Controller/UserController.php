<?php

declare(strict_types=1);

namespace InfrastructureLogic\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use ArchitectureLogic\Controller\DefaultRestController;
use DomainLogic\Entity\User\User;
use DomainLogic\Entity\User\UserRepository;

class UserController extends DefaultRestController
{
    /**
     * Get user by id
     *
     * @Serialize("default")
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function getById(Request $request, Response $response, array $args): Response
    {
        /**
         * @var $userRepository UserRepository
         */
        $userRepository = $this->getRepository(User::class);
        $user = $userRepository->find($args['id']);

        if (!$user instanceof User) {
            return $this->response(
                $response,
                DefaultRestController::HTTP_NOT_FOUND,
                'User does not exist'
            );
        }

        return $this->response(
            $response,
            DefaultRestController::HTTP_OK,
            'User',
            $user
        );
    }
}
