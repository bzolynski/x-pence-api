<?php

declare(strict_types=1);

namespace InfrastructureLogic\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use ArchitectureLogic\Controller\DefaultRestController;
use DomainLogic\Entity\ExpenseEntry\ExpenseEntry;
use DomainLogic\Entity\ExpenseEntry\ExpenseEntryRepository;
use DomainLogic\Entity\User\User;
use ArchitectureLogic\ValueObject\Money;

class ExpenseEntryController extends DefaultRestController
{
    /**
     * Get expense entry by id
     *
     * @Serialize("default")
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function getById(Request $request, Response $response, array $args): Response
    {
        /**
         * @var $expenseEntryRepository ExpenseEntryRepository
         */
        $expenseEntryRepository = $this->getRepository(ExpenseEntry::class);
        $expenseEntry = $expenseEntryRepository->find($args['id']);

        if (!$expenseEntry instanceof ExpenseEntry) {
            return $this->response(
                $response,
                DefaultRestController::HTTP_NOT_FOUND,
                'Expense entry does not exist'
            );
        }

        return $this->response(
            $response,
            DefaultRestController::HTTP_OK,
            'Expense Entry',
            $expenseEntry
        );
    }

    /**
     * Create expense entry
     *
     * @Serialize("default")
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function create(Request $request, Response $response, array $args): Response
    {
        $params = $request->getParsedBody();
        if (empty($params['value'])) {
            return $this->response(
                $response,
                DefaultRestController::HTTP_NOT_FOUND,
                'Parameter "value" is missing'
            );
        }
        $value = (float)$params['value'];
        $user = $this->entityManager->getReference(User::class, 1); // TODO: Hardcoded for now
        $expenseEntry = new ExpenseEntry($user, new Money($value));
        $this->entityManager->persist($expenseEntry);
        $this->entityManager->flush();

        return $this->response(
            $response,
            DefaultRestController::HTTP_OK,
            'Expense Entry',
            $expenseEntry
        );
    }
}
