<?php

declare(strict_types=1);

namespace ArchitectureLogic\Exception;

use Exception;

class NotAllowedValueException extends Exception
{

}
