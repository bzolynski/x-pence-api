<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\EventManager;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Gedmo\Timestampable\TimestampableListener;

class OrmBootstrap
{
    /**
     * @var ConfigurationBootstrap
     */
    private $config;

    /**
     * @var EntityManager
     */
    private $entityManager;

    private $cachedAnnotationReader;

    public function __construct(ConfigurationBootstrap $config)
    {
        $this->config = $config;
        AnnotationRegistry::registerLoader('class_exists');
    }

    public function getEntityManager(): EntityManager
    {
        if ($this->entityManager instanceof EntityManager) {
            return $this->entityManager;
        }

        $isDatabaseDebugMode = $this->config->getDatabaseDebugMode();
        $entityConfig = Setup::createAnnotationMetadataConfiguration(
            array($this->config->getAbsolutePath() . $this->config->getEntitiesDirectory()),
            $isDatabaseDebugMode,
            null,
            null,
            false
        );

        $eventManager = $this->getEventManager();

        $this->entityManager = EntityManager::create(
            $this->config->getDatabaseConnectionParameters(),
            $entityConfig,
            $eventManager
        );
        return $this->entityManager;
    }

    protected function getEventManager(): EventManager
    {
        $eventManager = new EventManager();

        // Add Timestampable event
        $timestampableListener = new TimestampableListener();
        $timestampableListener->setAnnotationReader($this->getCachedAnnotationReader());
        $eventManager->addEventSubscriber($timestampableListener);

        return $eventManager;
    }

    protected function getCachedAnnotationReader(): CachedReader
    {
        if ($this->cachedAnnotationReader instanceof CachedReader) {
            return $this->cachedAnnotationReader;
        }

        // TODO: Globally used cache driver, in production use APC or memcached
        $cache = new ArrayCache();

        $annotationReader = new AnnotationReader();
        $this->cachedAnnotationReader = new CachedReader(
            $annotationReader,
            $cache
        );
        return $this->cachedAnnotationReader;
    }
}
