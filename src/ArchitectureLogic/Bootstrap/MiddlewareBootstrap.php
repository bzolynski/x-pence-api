<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap;

use Slim\App;
use ArchitectureLogic\Middleware\MiddlewareInterface;

class MiddlewareBootstrap
{
    /**
     * @var App
     */
    private $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function attach(MiddlewareInterface $instance)
    {
        $this->app->add($instance);
    }
}
