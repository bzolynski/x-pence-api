<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap\Container;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use ArchitectureLogic\Controller\DefaultRestController;
use stdClass;
use Closure;

class NotFoundContainer implements ContainerInterface
{
    public function __construct()
    {

    }

    public function get(): Closure
    {
        return function(Container $container) {
            return function (Request $request, Response $response) use ($container) {

                if ($this->isApiPath($request->getUri()->getPath())) {
                    return $container['response']
                        ->withStatus(DefaultRestController::HTTP_BAD_REQUEST)
                        ->withJson($this->getApiErrorMessage());
                }

                $response = $response->withStatus(DefaultRestController::HTTP_NOT_FOUND);
                return $container->get('view')->render(
                    $response,
                    '404-page-not-found.twig'
                );
            };
        };
    }

    protected function getApiErrorMessage(): stdClass
    {
        $apiError = new stdClass();
        $apiError->message = "API method doesn't exists";
        return $apiError;
    }

    protected function isApiPath(string $uriPath): bool
    {
        return substr($uriPath, 0, 5) === '/api/';
    }
}
