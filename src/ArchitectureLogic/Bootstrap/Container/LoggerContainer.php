<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap\Container;

use ArchitectureLogic\Bootstrap\ConfigurationBootstrap;
use ArchitectureLogic\Service\LoggerService;
use Psr\Log\LoggerInterface;

class LoggerContainer implements ContainerInterface
{
    /**
     * @var ConfigurationBootstrap
     */
    private $config;

    public function __construct(ConfigurationBootstrap $config)
    {
        $this->config = $config;
    }

    public function get(): LoggerInterface
    {
        $logger = new LoggerService($this->config->getAbsolutePath() . $this->config->getLogsDirectory());
        return $logger->getInstance();
    }
}
