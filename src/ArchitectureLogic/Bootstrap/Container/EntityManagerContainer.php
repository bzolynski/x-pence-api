<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap\Container;

use Doctrine\Orm\EntityManager;

class EntityManagerContainer implements ContainerInterface
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function get(): EntityManager
    {
        return $this->entityManager;
    }
}
