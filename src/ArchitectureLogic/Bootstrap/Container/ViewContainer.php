<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap\Container;

use ArchitectureLogic\Bootstrap\ConfigurationBootstrap;
use Slim\Views\Twig;

class ViewContainer implements ContainerInterface
{
    /**
     * @var ConfigurationBootstrap
     */
    private $config;

    public function __construct(ConfigurationBootstrap $config)
    {
        $this->config = $config;
    }

    public function get(): Twig
    {
        $view = new Twig($this->config->getAbsolutePath() . 'src/DomainLogic/Templates', array(
            'debug' => $this->config->getViewDebugMode()
        ));
        return $view;
    }
}
