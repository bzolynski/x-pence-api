<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap\Container;

use ArchitectureLogic\Bootstrap\ConfigurationBootstrap;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Serializer;

class SerializerContainer implements ContainerInterface
{
    /**
     * @var ConfigurationBootstrap
     */
    private $config;

    public function __construct(ConfigurationBootstrap $config)
    {
        $this->config = $config;
    }

    public function get(): Serializer
    {
        $debugForSerializer = $this->config->getSerializerDebugMode();
        $serializer = SerializerBuilder::create()
            ->setCacheDir($this->config->getAbsolutePath() . $this->config->getSerializerCacheDirectory())
            ->setDebug($debugForSerializer)
            ->build();
        return $serializer;
    }
}
