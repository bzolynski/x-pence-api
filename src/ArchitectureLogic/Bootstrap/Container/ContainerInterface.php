<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap\Container;

interface ContainerInterface
{
    public function get();
}
