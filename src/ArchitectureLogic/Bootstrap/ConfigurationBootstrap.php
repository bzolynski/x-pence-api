<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap;

use Symfony\Component\Yaml\Parser;

class ConfigurationBootstrap
{
    /**
     * @var array
     */
    private $parameters;

    /**
     * @var string
     */
    private $absolutePath;

    public function __construct($configurationFile, $absolutePath = '')
    {
        $this->absolutePath = $absolutePath;
        $this->setParametersFromYamlFile($absolutePath . $configurationFile);
    }

    public function getDatabaseConnectionParameters(): array
    {
        return array(
            'dbname' => $this->parameters['db_name'],
            'user' => $this->parameters['db_user'],
            'password' => $this->parameters['db_pass'],
            'host' => $this->parameters['db_host'],
            'driver' => $this->parameters['db_driver'],
        );
    }

    public function getSlimDebugMode(): bool
    {
        return $this->parameters['slim_debug'];
    }

    public function getDatabaseDebugMode(): bool
    {
        return $this->parameters['database_debug'];
    }

    public function getSerializerDebugMode(): bool
    {
        return $this->parameters['serializer_debug'];
    }

    public function getSerializerCacheDirectory(): string
    {
        return $this->parameters['serializer_cache_directory'];
    }

    public function getLogsDirectory(): string
    {
        return $this->parameters['logs_directory'];
    }

    public function getEntitiesDirectory(): string
    {
        return $this->parameters['entities_directory'];
    }

    public function getViewDebugMode(): bool
    {
        return $this->parameters['view_debug'];
    }

    public function getSessionLifetime(): int
    {
        return $this->parameters['session_lifetime'];
    }

    public function getAbsolutePath(): string
    {
        return $this->absolutePath;
    }

    protected function setParametersFromYamlFile(string $configurationFile)
    {
        $yaml = new Parser();
        $parametersTable = $yaml->parse(file_get_contents($configurationFile));
        $this->parameters = $parametersTable['parameters'];
    }
}
