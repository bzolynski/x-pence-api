<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap;

use ArchitectureLogic\Bootstrap\Container\ContainerInterface;
use Slim\Container;

class ContainerBootstrap
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $containers;

    /**
     * @var ConfigurationBootstrap
     */
    private $config;

    public function __construct(ConfigurationBootstrap $config)
    {
        $this->config = $config;
        $this->containers = array();
        $this->instantiateContainer();
    }

    public function attach(string $key, ContainerInterface $container)
    {
        $this->container[$key] = $container->get();
    }

    public function getContainer(): Container
    {
        return $this->container;
    }

    protected function instantiateContainer()
    {
        $configuration = [
            'settings' => [
                'displayErrorDetails' => $this->config->getSlimDebugMode()
            ],
        ];
        $this->container = new Container($configuration);
    }
}
