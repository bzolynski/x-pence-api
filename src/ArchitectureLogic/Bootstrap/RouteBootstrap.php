<?php

declare(strict_types=1);

namespace ArchitectureLogic\Bootstrap;

use Slim\App;
use ArchitectureLogic\Service\YamlRouteService;

class RouteBootstrap
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var YamlRouteService
     */
    private $routeService;

    public function __construct(App $app, ConfigurationBootstrap $config)
    {
        $this->app = $app;
        $this->routeService = new YamlRouteService($config->getAbsolutePath() . 'config/routing.yml');
        $this->setRoutes();
    }

    public function getAppWithRoutes(): App
    {
        return $this->app;
    }

    protected function setRoutes()
    {
        $routes = $this->routeService->getAll();
        foreach ($routes as $route) {
            $method = $route['method'];
            $this->app->$method($route['path'], $route['action']);
        }
    }
}
