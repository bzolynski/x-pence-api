<?php

declare(strict_types=1);

namespace ArchitectureLogic\ValueObject;

use ArchitectureLogic\Exception\NotAllowedValueException;

class Money
{
    protected $value;

    /**
     * @param float $value
     */
    public function __construct(float $value)
    {
        $this->setValue($value);
    }

    /**
     * @param float $value
     * @throws NotAllowedValueException
     */
    private function setValue(float $value)
    {
        if (!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', (string)$value)) {
            throw new NotAllowedValueException($value);
        }
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }
}
