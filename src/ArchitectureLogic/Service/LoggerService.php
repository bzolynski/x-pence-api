<?php

declare(strict_types=1);

namespace ArchitectureLogic\Service;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Processor\WebProcessor;
use Monolog\Processor\IntrospectionProcessor;
use Psr\Log\LoggerInterface;

class LoggerService
{
    const DEFAULT_LINE_FORMAT = "[%datetime%] %channel%.%level_name%: %message% %extra% %context%\n";

    protected $logger;

    public function __construct($logsDirectory = null)
    {
        if (!$logsDirectory) {
            $logsDirectory = '/var/log/php';
        }
        $logsDirectory = rtrim($logsDirectory, '/');
        $handlers = array(
            array(
                'path' => $logsDirectory . '/warning.log',
                'level' => Logger::WARNING
            ),
            array(
                'path' => $logsDirectory . '/error.log',
                'level' => Logger::ERROR
            ),
            array(
                'path' => $logsDirectory . '/critical.log',
                'level' => Logger::CRITICAL
            ),
            array(
                'path' => $logsDirectory . '/debug.log',
                'level' => Logger::DEBUG
            )
        );

        $logger = new Logger('Request');

        foreach($handlers as $handler) {
            $streamHandler = new StreamHandler($handler['path'], $handler['level']);
            $streamHandler->setFormatter(new LineFormatter(self::DEFAULT_LINE_FORMAT));

            $introspectionProcessor = new IntrospectionProcessor($handler['level'], array('Monolog\\', 'DefaultRestController'));
            $webProcessor = new WebProcessor();
            $streamHandler
                ->pushProcessor($introspectionProcessor)
                ->pushProcessor($webProcessor);

            $logger->pushHandler($streamHandler);
        }

        $this->logger = $logger;
    }

    public function getInstance(): LoggerInterface
    {
        return $this->logger;
    }
}
