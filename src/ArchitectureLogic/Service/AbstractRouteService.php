<?php

declare(strict_types=1);

namespace ArchitectureLogic\Service;

use Exception;

abstract class AbstractRouteService
{
    /**
     * @var string
     */
    protected $routeConfigFile;

    /**
     * List of allowed HTTP methods
     *
     * @var array
     */
    private $methodsAllowed = array('get', 'post', 'put', 'delete', 'patch', 'head');

    /**
     * List of routes
     *
     * @var array
     */
    protected $routes = array();

    /**
     * Parse configuration in YAML format
     *
     * @param $routeConfigFile
     * @throws Exception
     */
    public function __construct($routeConfigFile)
    {
        $this->routeConfigFile = $routeConfigFile;
        $this->parse();
    }

    /**
     * Returns all routes
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->routes;
    }

    /**
     * Returns data for specific route URL
     *
     * @param string $routeUrl
     * @throws Exception
     * @return string
     */
    public function get($routeUrl): string
    {
        if (isset($this->routes[$routeUrl])) {
            return $this->routes[$routeUrl];
        } else {
            throw new Exception('Route with URL: "' . $routeUrl . '" not defined error.');
        }
    }

    /**
     * Retrieve routes from configuration array
     *
     * @param array $data
     * @return array
     * @throws Exception
     */
    protected function retrieveRoutes(array $data): array
    {
        $routes = array();

        foreach ($data as $routeName=>$route) {
            if (isset($route['path']) && $route['path']) {
                $path = $route['path'];
            } else {
                throw new Exception('Path not found for "' . $routeName . '" route.');
            }

            if (isset($route['action']) && $route['action']) {
                if (!strpos($route['action'], ':')) {
                    throw new Exception('Controller and function name must be separated by semicolon in action for "' . $routeName . '".');
                }
                $action = $route['action'];
            } else {
                throw new Exception('Action not found for "' . $routeName . '" route.');
            }

            if (isset($route['method']) && $route['method']) {
                if (!in_array(strtolower($route['method']), $this->methodsAllowed)) {
                    throw new Exception('Method not allowed for "' . $routeName . '" route.');
                }
                $method = strtolower($route['method']);
            } else {
                throw new Exception('Method not found for "' . $routeName . '" route.');
            }

            $routes[] = array(
                'path' => $path,
                'method' => $method,
                'action' => $action
            );
        }

        return $routes;
    }

    protected function parse()
    {

    }
}
