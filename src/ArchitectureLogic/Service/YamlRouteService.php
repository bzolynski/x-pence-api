<?php

declare(strict_types=1);

namespace ArchitectureLogic\Service;

use Symfony\Component\Yaml\Parser;
use Exception;

class YamlRouteService extends AbstractRouteService
{
    /**
     * Parse configuration in YAML format
     *
     * @param string $routeConfigFile
     * @throws Exception
     */
    public function __construct($routeConfigFile)
    {
        parent::__construct($routeConfigFile);
    }

    /**
     * @throws Exception
     */
    protected function parse(): bool
    {
        if (is_file($this->routeConfigFile)) {
            $yamlParser = new Parser();
            $data = $yamlParser->parse(file_get_contents($this->routeConfigFile));
            $this->routes = $this->retrieveRoutes($data);
            return true;
        }
        throw new Exception('Route configuration file "' . $this->routeConfigFile .  '" not found error.');
    }
}
