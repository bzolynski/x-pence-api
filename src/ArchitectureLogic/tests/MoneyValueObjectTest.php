<?php

declare(strict_types=1);

namespace ArchitectureLogic\Test;

use ArchitectureLogic\ValueObject\Money;

class TestExpenseValueObject extends \PHPUnit_Framework_TestCase
{
    public function allowedValues()
    {
        return array(
            array(150),
            array(150.56),
            array(150.4)
        );
    }

    public function notAllowedValues()
    {
        return array(
            array('150,5'),
            array(150.125),
            array(-150),
            array('150M'),
            array('A')
        );
    }

    public function testInstantiateObject()
    {
        $expense = new Money(150);
        $this->assertInstanceOf(Money::class, $expense);
    }

    /**
     * @dataProvider allowedValues
     * @param $value
     */
    public function testAllowedValues($value)
    {
        $expense = new Money($value);
        $this->assertEquals($value, $expense->getValue());
    }

    /**
     * @dataProvider notAllowedValues
     * @param $value
     * @throws \ArchitectureLogic\Exception\NotAllowedValueException
     */
    public function testNotAllowedValues($value)
    {
        $this->setExpectedException('\Error');
        new Money($value);
    }
}
