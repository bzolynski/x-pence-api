<?php

declare(strict_types=1);

namespace ArchitectureLogic\Middleware;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

interface MiddlewareInterface
{
    public function __invoke(Request $request, Response $response, App $next);
}
