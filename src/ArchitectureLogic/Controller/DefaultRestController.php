<?php

declare(strict_types=1);

namespace ArchitectureLogic\Controller;

use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Slim\Http\Response;
use Slim\Container;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializationContext;
use ArchitectureLogic\Handler\Annotation\SerializerAnnotation;
use Monolog\Logger;

abstract class DefaultRestController
{
    private $serializerGroups = null;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Serializer
     */
    protected $serializer;

    protected $logger;

    const HTTP_OK = 200;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_NOT_FOUND = 404;

    public function __construct(Container $container)
    {
        $this->entityManager = $container->get('entityManager');
        $this->serializer = $container->get('serializer');
        $this->logger = $container->get('logger');
    }

    /**
     * @param Response $response
     * @param int $responseCode
     * @param string $message
     * @param mixed $data
     * @param array $errors
     *
     * @return Response
     */
    public function response(Response $response, $responseCode, $message, $data = null, $errors = null): Response
    {
        if ($responseCode === self::HTTP_NOT_FOUND) {
            /** @var Logger $logger */
            $logger = $this->getLogger();
            $logger->addCritical($message, array($data));
        }

        $responseData = array(
            'responseCode' => $responseCode,
            'message' => $message
        );

        if (!is_null($data)) {
            $responseData['data'] = $data;
        }

        if (!is_null($errors)) {
            $responseData['errors'] = $errors;
        }

        $jsonContent = $this->serializer->serialize($responseData, 'json', $this->serializerGroups);

        $response = $response->withJson(json_decode($jsonContent), $responseCode);
        return $response;
    }

    /**
     * @param string $repositoryName
     * @return EntityRepository
     */
    public function getRepository($repositoryName): EntityRepository
    {
        $serializerAnnotation = new SerializerAnnotation();
        $serializerGroups = $serializerAnnotation->get();

        if (!empty($serializerGroups)) {
            $this->setSerializerGroups($serializerGroups);
        }

        return $this->entityManager->getRepository($repositoryName);
    }

    public function setSerializerGroups(array $serializerGroups)
    {
        $this->serializerGroups = SerializationContext::create()->setGroups($serializerGroups);
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}
