<?php

declare(strict_types=1);

namespace ArchitectureLogic\Handler\Annotation;

/**
 * Get annotations from PHPDoc
 *
 * Class AbstractAnnotation
 * @package ArchitectureLogic\Helper\Annotation
 */
abstract class AbstractAnnotation
{
    protected $docComment;

    public function __construct()
    {
        $callers = debug_backtrace();
        $function = $callers[2]['function'];
        $class = $callers[2]['class'];

        $oReflectionClass = new \ReflectionMethod($class, $function);
        $this->docComment = $oReflectionClass->getDocComment();
    }
}
