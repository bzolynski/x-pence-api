<?php

declare(strict_types=1);

namespace ArchitectureLogic\Handler\Annotation;

/**
 * Search for @Serialize() annotation in PHPDoc
 *
 * Class SerializerAnnotation
 * @package ArchitectureLogic\Helper\Annotation
 */
class SerializerAnnotation extends AbstractAnnotation
{
    public function get(): array
    {
        preg_match_all('/@Serialize\((.*?)\)[\n|\r]/s', $this->docComment, $annotations);
        if (!empty($annotations) && isset($annotations[1]) && is_array($annotations[1]) && isset($annotations[1][0])) {
            $result = $annotations[1][0];
            return json_decode('[' . $result . ']', true);
        } else {
            return array();
        }
    }
}
