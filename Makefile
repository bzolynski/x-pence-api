all : composer
.PHONY: all start stop composer phpunit db-drop db-create db-fixtures

start :
	docker-compose up -d

stop :
	docker-compose down

composer :
	docker-compose run --rm --no-deps php composer install

phpunit-architecture :
	docker-compose run --rm --no-deps php vendor/bin/phpunit --testsuite ArchitectureLogic

phpunit-domain :
	docker-compose run --rm --no-deps php vendor/bin/phpunit --testsuite DomainLogic

db-drop :
	docker-compose run --rm --no-deps php php doctrine.php orm:schema-tool:drop --force

db-create :
	docker-compose run --rm --no-deps php php doctrine.php orm:schema-tool:create

db-update :
	docker-compose run --rm --no-deps php php doctrine.php orm:schema-tool:update --force

db-fixtures :
	docker-compose run --rm --no-deps php php doctrine.php doctrine:fixtures:load

behat-all :
	docker-compose exec php sh -c "./vendor/bin/behat --config behat/config.yml"

behat-tags :
	docker-compose exec php sh -c "./vendor/bin/behat --config behat/config.yml --tags $(tags)"
