# X-Pence API

This is an API for X-Pence project, written in PHP scripting language. Android app codebase can be found under: https://bitbucket.org/bzolynski/x-pence

# Purpose

In a _Code First_ approach which is the essential for Domain-driven Design, you can delay the decision to choose a persistence layer.
The main advantage is to be able to focus on the domain design rather than design database first and then create the classes
which match database design. You can just start to write an application by creating classes, without having to wait.
But before the whole code will be written, another feature will be used. Behaviour-driven Development (BDD aka _Specification
By Example_) helps to define all requirements before development will be started. It works also as living documentation,
which can prevent a lot of issues during the project's lifetime.

In this project, I will try to create the fusion between Behaviour-driven Development, Domain-driven Design and
Test-driven Development. Actually it's not the overload, as all these approaches have the one common aim: to create
the high quality software. By defining the requirements first (features), it's much easier to start working on the
Domain layer, and it's clear what we need to develop, what functionality is needed. And it helps to see the big picture.
Test-driven Development helps to create good classes structure (entities), and by using Domain-driven Design we don't have
to work with database as we can use in-memory repositories. This way it's much faster to re-design the architecture, or
refactor it later, and see results and new ideas much quicker. And eventually, switching between in-memory repository / database
repository is straightforward. I will show how easy is to switch between them using the same entities structure,
the same repositories methods and even the same data fixtures if we want! And the concrete persistence layer implementation
can be delayed to the very last moment, so on the end is much clearer to decide which database engine will suits us the most,
and which ORM (if any) we want to use. Regarding this project, I will focus on _Doctrine 2 ORM_, one of the best ORM tools
in the PHP world, but as the _DomainLogic_ layer and _InfrastructureLogic_ layer are separated, nothing can't stop us
to change the database engine.


# In details
- There are two kind of entities, they are located in _DomainLogic_ layer and_InfrastructureLogic_ layer.
- There are two kind of repositories, they are located in _DomainLogic_ layer and _InfrastructureLogic_ layer, the same as for entities.
- _DomainLogic_ repositories are in-memory repositories used in TDD tests, and in BDD specification, created in the Domain-driven Design process.
- At the very beginning we need to create the specification and go through required functionalities.
- Then, in domain design stage I will use _DomainLogic_ entities, and here I will implement getters and setters methods.
- When domain design will be finished for particular features, we can check if these requirements has been fulfilled by
testing the specification, this is our living documentation which help us to see if everything is still working properly.
- Then we can move to the next feature, one after another.
- At the final stage of the whole project, database implementation can take place. We will create entities in _InfrastructureLogic_ layer,
which will extend entities from _DomainLogic_ layer. This way we can override getters and setters methods for real persistence purposes.
- We will add _Doctrine_ annotations to _InfrastructureLogic_ entities.
- _InfrastructureLogic_ repositories are used for production, and they are an implementation of real database repositories,
e.g. using _Doctrine ORM_ database layer. However, with data fixtures, we can use them for integration tests.
- API endpoints are using _InfrastructureLogic_ repositories.
- API endpoints are defined in _InfrastructureLogic_ layer in separate controllers files.
- Response data can be serialized into groups using _JMS Serializer_.
- _InfrastructureLogic_ will contain integration tests to check real database implementation. We will test API endpoints
using test database. Read more below why I would recommend this approach.
- In-memory repositories data from _DomainLogic_ layer can be used for data fixtures in _InfrastructureLogic_. It's very comfortable
to use the same data for in-memory and real database as we need to write & set up it only once. It's less coupled this way,
but we don't need to bother, fixtures were written in domain design stage for in-memory repositories so we will just
use them in the database implementation stage. And they are used just for test purposes, so we can skip this step, but
it's worth to add them though...


# Layers

## Architecture Logic

This layer contains common classes which can be shared through other applications.
It contains exceptions, value objects, and other common components. It's worth here to create unit tests for them.


## Domain Logic

This is our Domain-driven Design layer.
It contains in-memory repositories which will be used during domain design development process, but we'll use them also for unit tests.
Example of in-memory repository: `DomainLogic\Entity\ExpenseEntry\InMemoryExpenseEntryRepository`.

They contains getters and setters methods. Note that we can create an in-memory object and set `id` with public `setId()` method what
is not allowed from _InfrastructureLogic_ layer (persistence layer).


## Infrastructure Logic

This layer contains technical services, persistence, and more generally, concrete implementations of the _DomainLogic_ layer.
Controllers with API endpoints were included here also, as they are hitting database layer.

What's most important, _InfrastructureLogic_ layer contains implementations of _Doctrine ORM_ repositories as well as body of entities,
with _Doctrine ORM_ annotations. Repositories in _InfrastructureLogic_ must reflect to these from _DomainLogic_
layer as they are real implementations of domain repositories. That's why they extends domain entities.

As this layer contains database persistence logic, data fixtures has been added here also.


### Integration tests?

_Infrastructure_Logic_ layer will contain also integration tests to check database persistence with data fixtures.
These tests are much slower and harder to write, but in my opinion, they are very important to be sure, that our application will
work well in the production environment. There are a lot of opinions, that testing real database is not recommended, but
we can add few crucial tests by calling API endpoints to be sure, that the application will behave properly in the real environment,
i.e. this way we can check failures like UNIQUE field constraint error in MySQL.


### Behaviour-driven Development, how?

Note that I will use Behaviour-driven Development approach only for defining the requirements and to be sure, that
domain is working properly during the project's lifetime, but I'm not going to use it for integration tests as
it's too slow and this kind of tests can flicker (unexpected results regarding some services latency, etc.).
For the same reasons, I'm not going to test User Interface. There are two contexts, which will be tested:
Controller Context (where it will be possible to test some HTML elements), and Domain Context (business logic).

All specifications are defined in `features/` folder, and all contexts in `features/bootstrap/` folder.


# Technical specification

- PHP 7 with nginx
- Slim 3 framework
- Docker (Docker Compose used)
- Doctrine 2 ORM (MySQL)
- Doctrine data fixtures
- JMS Serializer
- PHPUnit for unit tests
- Behat for BDD specification and tests
- Monolog logger
- YAML Reader
- Incenteev parameter handler
- Symfony Command
- Timestampable (Gedmo) Doctrine annotation for created date fields
- Twig template


# Installation

For development environment, Docker has been used with _docker-compose_. 
It requires images for nginx, PHP and MySQL (`Dockerfile`s are included in "docker/" directory).

## Notes before installation:

- Code will be attached as the separate data container.
- HTTP is exposed from container on port 80, and MySQL on port 3306.
- Because MySQL container is linked to PHP container, MySQL is resolved by "mysql" hostname for DB connection.
- Use default parameters for MySQL connection, and if not, set the same values in `parameters.yml` and in `docker-compose.yml` files.
- MySQL database will be persisted to `database/` folder and attached as the volume to `/var/lib/mysql`
- All logs from Monolog will be written as relative path to "logs/" directory which is configurable in `parameters.yml`.
- Relative path to "cache/jms-serializer" directory has been used for JMS Serializer. It's configurable in `parameters.yml` also.

## Installation process:

1. Install Docker engine (at least in version 1.10.0) and _docker-compose_ (at least in version 1.7.0 as `exec` command has been used).
2. Build containers by executing: `docker/build.sh` bash command.
3. Run `docker-compose up` or `make start` in main directory which reads `docker-compose.yml` file.
4. Install composer dependencies by running `make composer` (you will be asked for parameters, just choose defaults).
5. Load database fixtures if you want to test API with DB:
    - `make db-create`
    - `make db-fixtures`


# Running

1. `make start` to run Docker containers.
2. `make stop` to stop running Docker.
3. `docker-compose logs` to see logs from containers.


# Testing

    phpunit --testsuite ArchitectureLogic
    phpunit --testsuite DomainLogic

  or using _make_:
  
    make phpunit-architecture
    make phpunit-domain


# ORM

## Dropping database schema

    php doctrine.php orm:schema-tool:drop --force
    
  or using _make_:
  
    make db-drop

## Creating database schema

    php doctrine.php orm:schema-tool:create

  or using _make_:
  
    make db-create

## Update database schema

    php doctrine.php orm:schema-tool:create

  or using _make_:
  
    make db-update

## Loading data fixtures to database

    php doctrine.php doctrine:fixtures:load

  or using _make_:
  
    make db-fixtures
