<?php

use Slim\App;
use ArchitectureLogic\Bootstrap\ConfigurationBootstrap;
use ArchitectureLogic\Bootstrap\OrmBootstrap;
use ArchitectureLogic\Bootstrap\ContainerBootstrap;
use ArchitectureLogic\Bootstrap\RouteBootstrap;
use ArchitectureLogic\Bootstrap\Container\EntityManagerContainer;
use ArchitectureLogic\Bootstrap\Container\SerializerContainer;
use ArchitectureLogic\Bootstrap\Container\LoggerContainer;
use ArchitectureLogic\Bootstrap\Container\ViewContainer;
use ArchitectureLogic\Bootstrap\Container\NotFoundContainer;

$absolutePath = '../';

$config = new ConfigurationBootstrap('config/parameters.yml', $absolutePath);

$ormBootstrap = new OrmBootstrap($config);
$containerBootstrap = new ContainerBootstrap($config);
$containerBootstrap->attach('entityManager', new EntityManagerContainer($ormBootstrap->getEntityManager()));
$containerBootstrap->attach('serializer', new SerializerContainer($config));
$containerBootstrap->attach('logger', new LoggerContainer($config));
$containerBootstrap->attach('view', new ViewContainer($config));
$containerBootstrap->attach('notFoundHandler', new NotFoundContainer());

$container = $containerBootstrap->getContainer();
$app = new App($container);
$routes = new RouteBootstrap($app, $config);
$app = $routes->getAppWithRoutes();
