<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use ArchitectureLogic\Bootstrap\ConfigurationBootstrap;
use ArchitectureLogic\Bootstrap\OrmBootstrap;

$absolutePath = '';

$config = new ConfigurationBootstrap('config/parameters.yml', $absolutePath);

$orm = new OrmBootstrap($config);
$entityManager = $orm->getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);
