Feature: Set default categories when new group has been created
  As a group creator
  I want to have default categories already set
  So I don't have to add them from scratch

  Scenario Outline: Set default categories when new group has been created
    Given I created the new group
    When I request the categories list
    Then I see category titled "<category>"

    Examples:
      | category  |
      | Groceries |
      | Going out |
      | Transport |
      | Shopping  |
      | Other     |
