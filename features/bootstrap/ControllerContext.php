<?php
declare(strict_types=1);
use Pavlakis\Slim\Behat\Context\App;
use Behat\Behat\Tester\Exception\PendingException;

/**
 * Defines application features for static pages by mocking requests in controllers (no JavaScript interaction)
 */
class ControllerContext extends ControllerBaseContext
{
    use App;
}
