<?php

declare(strict_types=1);

use Behat\Behat\Context\SnippetAcceptingContext;
use Pavlakis\Slim\Behat\Context\App;
use Pavlakis\Slim\Behat\Context\KernelAwareContext;

/**
 * Defines application features for static pages by mocking requests in controllers (no JavaScript interaction)
 */
class ControllerBaseContext implements SnippetAcceptingContext, KernelAwareContext
{
    use App;

    /** @var Slim\Http\Response */
    protected $response;

    /**
     * @param string $message
     * @throws Exception
     */
    public function throwExpectationException($message)
    {
        throw new Exception($message);
    }
}
