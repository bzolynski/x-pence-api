<?php

declare(strict_types=1);

use Behat\Behat\Context\SnippetAcceptingContext;

/**
 * Defines domain layer features
 */
class DomainBaseContext implements SnippetAcceptingContext
{

}
