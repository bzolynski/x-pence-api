<?php

use Symfony\Component\Console\Helper\HelperSet;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Symfony\Component\Console\Application;

use ArchitectureLogic\Console\Command\LoadDataFixturesDoctrineCommand;

require_once 'vendor/autoload.php';

$configFile = 'cli-config.php';

if (!file_exists($configFile)) {
    ConsoleRunner::printCliConfigTemplate();
    exit(1);
}

if (!is_readable($configFile)) {
    echo 'Configuration file [' . $configFile . '] does not have read permission.' . "\n";
    exit(1);
}

$commands = array();

$helperSet = require_once $configFile;

if (!$helperSet instanceof HelperSet) {
    foreach ($GLOBALS as $helperSetCandidate) {
        if ($helperSetCandidate instanceof HelperSet) {
            $helperSet = $helperSetCandidate;
            break;
        }
    }
}

$cli = new Application('Doctrine Command Line Interface', Doctrine\ORM\Version::VERSION);
$cli->setCatchExceptions(true);
$cli->setHelperSet($helperSet);
ConsoleRunner::addCommands($cli);

// Add custom commands
$cli->add(new LoadDataFixturesDoctrineCommand());

$cli->run();

Doctrine\ORM\Tools\Console\ConsoleRunner::run($helperSet, $commands);
